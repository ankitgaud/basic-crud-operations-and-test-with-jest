var express = require('express');
var router = express.Router();

const customer = require("../model/customer");
const product = require("../model/product");
const order = require("../model/orders");

router.get('/customer', async function (req, res) {
  return res.send({
    message: "successful",
    status: "200",
    results: await customer.fetch_customer(req.query)
  })
});

router.get('/product', async (req, res) => {
  try {
    return res.send({
      message: "successful",
      status: "200",
      results: await product.fetch_product(req.query)
    })
  } catch (error) {
    throw error
  }
})

router.get('/order', async (req, res) => {
  try {
    return res.send({
      message: "successful",
      status: "200",
      results: await order.fetch_order(req.query)
    })
  } catch (error) {
    throw error
  }
})

router.put('/product/:id', async (req, res)=>{
  try{
    return res.send({
      message: "successful",
      status: "200",
      results: await product.update_product(req.body, req.params.id)
    })
  }catch(err){
    throw err
  }
})

router.put('/order/:id', async (req, res)=>{
  try{
    return res.send({
      message: "successful",
      status: "200",
      results: await order.update_order(req.body, req.params.id)
    })
  }catch(err){
    throw err
  }
})

module.exports = router;
