class Orders {
    constructor(
        id,
        product_id,
        customer_id,
        delivered_at,
        pickup_store,
        status,
        created_at,
        updated_at
    ) {
        this.id = id;
        this.product_id = product_id;
        this.customer_id = customer_id;
        this.delivered_at = delivered_at;
        this.pickup_store = pickup_store;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}

module.exports = {
    Orders: Orders
}