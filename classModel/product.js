class Products {
    constructor(
        id,
        name,
        brand,
        model,
        price,
        extra_detail,
        status,
        created_at,
        updated_at
    ) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.model = model;
        this.price = price;
        this.extra_detail = extra_detail;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at
    }
}

module.exports = {
    Products: Products
}