class Customers {
    constructor(
        id,
        name,
        email,
        password,
        dob,
        address,
        premium,
        status,
        created_at,
        updated_at
    ) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.dob = dob;
        this.address = address;
        this.premium = premium;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}

module.exports = {
    Customers: Customers
}