  
const { createConnection } = require("typeorm");
require('dotenv').config()

BaseConnection = async () =>{
    const connection = await createConnection({
        type: "mysql",
        host: "localhost",
        port: "3306",
        username: "root",
        password: "",
        database: "yash",
        synchronize: false,
        logging: false,
        entities: [
            require('./entity/customerSchema'),
            require("./entity/orderSchema"),
            require("./entity/productSchema")
        ]
    });
    return connection;
}

module.exports = BaseConnection();