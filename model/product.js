const { getConnection } = require('typeorm');
const Products = require("../classModel/product").Products;

const fetch_product = async (payload) => {
    try{
        var obj = ((payload.name !== "") ? `pro.status = "1" and pro.name like '%${payload.name}%'` : `pro.status = '1'`);
        obj = ((payload.brand !== "" && payload.name !== "") ? `pro.status = "1" and pro.name like '%${payload.name}%' and pro.brand = '${payload.brand}'` : `${obj}`);
        obj = ((payload.brand !== "" && payload.name !== "" && payload.model !== "") ? `pro.status = "1" and pro.name like '%${payload.name}%' and pro.brand = '${payload.brand}' and pro.model = '${payload.model}'` : `${obj}`);

        let data = await getConnection()
            .getRepository(Products)
            .createQueryBuilder("pro")
            .where(obj)
            .select()
            .getMany();
        return data
    }catch(err){
        throw err
    }
}

const create_product = async (payload) =>{
    try{
        return "dhu"
    }catch(error){
        throw error
    }
}

const update_product = async (payload, id) => {
    try{
        let obj = {
            name: payload.name,
            brand: payload.brand,
            model: payload.model,
            price: payload.price,
            updated_at: new Date()
        }
        let data = await getConnection()
            .createQueryBuilder()
            .update(Products)
            .set(obj)
            .where("status = 1 and id = :id", {
                id: id
            })
            .execute()
        return data
    }catch(error){
        throw error
    }
}

module.exports = {
    fetch_product,
    create_product,
    update_product
}