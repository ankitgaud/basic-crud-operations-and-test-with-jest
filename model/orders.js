const { getConnection } = require('typeorm');
const Orders = require("../classModel/orders").Orders;
const Customers = require("../classModel/customer").Customers;
const Products = require("../classModel/product").Products;

const fetch_order = async (payload) => {
    try {
        var obj = ((payload.cust_id !== "" && payload.pro_id !== "") ? `pro.status = '1' and ord.status = "1" and cust.status = "1" and ord.product_id = '${payload.pro_id}' and ord.customer_id = '${payload.cust_id}'` : `ord.status = '1' and cust.status = '1' and pro.status = "1"`);
        obj = ((payload.cust_id !== "" && payload.pro_id == "") ? `pro.status = '1' and ord.status = "1" and cust.status = "1" and ord.customer_id = '${payload.cust_id}'` : `${obj}`);
        obj = ((payload.pro_id !== "" && payload.cust_id == "") ? `pro.status = '1' and ord.status = "1" and cust.status = "1" and ord.product_id = '${payload.pro_id}'` : `${obj}`);
        
        let data = await getConnection()
            .getRepository(Orders)
            .createQueryBuilder("ord")
            .leftJoin(Customers, "cust", "cust.id = ord.customer_id")
            .leftJoin(Products, "pro", "pro.id = ord.product_id")
            .where(obj)
            .select([
                "ord.id as order_id",
                "ord.product_id as product_id",
                "ord.delivered_at as delivered_at",
                "ord.pickup_store as pickup_store",
                "cust.id as customer_id",
                "cust.name as customer_name", 
                "cust.email as customer_email",
                "pro.id as product_id",
                "pro.name as product_name",
                "pro.brand as product_brand",
                "pro.model as product_mode",
                "pro.price as product_cost"
            ])
            .getRawMany();
        return data
    } catch (err) {
        throw err
    }
}

const create_order = async (payload) => {
    try {
        return "dhu"
    } catch (error) {
        throw error
    }
}

const update_order = async (payload, id) => {
    try {
        let obj = {
            product_id: payload.product_id,
            customer_id: payload.customer_id,
            delivered_at: payload.delivered_at,
            pickup_store: payload.pickup_store,
            updated_at: new Date()
        }
        let data = await getConnection()
            .createQueryBuilder()
            .update(Orders)
            .set(obj)
            .where("status = 1 and id = :id", {
                id: id
            })
            .execute()
        return data
    } catch (error) {
        throw error
    }
}

module.exports = {
    fetch_order,
    create_order,
    update_order
}