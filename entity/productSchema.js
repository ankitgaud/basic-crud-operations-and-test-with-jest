const { Timestamp } = require('typeorm');

const EntitySchema = require('typeorm').EntitySchema;
const Products = require('../classModel/product').Products;

module.exports = new EntitySchema({
    name: "products",
    target: Products,
    columns: {
        id : {
            primary: true,
            type: "bigint",
            generated: true
        },
        name: {
            type: "varchar", 
            nullable: true
        },
        brand: {
            type: "varchar",
            nullable: true
        },
        model: {
            type: "varchar",
            nullable: true
        },
        price:{
            type: "varchar",
            nullable: true
        },
        extra_detail: {
            type: "json",
            nullable: true
        },
        status: {
            type: "tinyint",
            nullable: false,
            default: 1
        },
        created_at: {
            type: "timestamp",
            nullable: true
        },
        updated_at: {
            type: "timestamp",
            nullable: true
        }
    }
})