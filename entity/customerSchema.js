const { Timestamp } = require('typeorm');

const EntitySchema = require('typeorm').EntitySchema;
const Customers = require('../classModel/customer').Customers;

module.exports = new EntitySchema({
    name: "Customers",
    target: Customers,
    columns: {
        id : {
            primary: true,
            type: "bigint",
            generated: true
        },
        name: {
            type: "varchar", 
            nullable: true
        },
        email: {
            type: "varchar",
            nullable: true
        },
        password: {
            type: "varchar",
            nullable: true
        },
        dob:{
            type: "timestamp",
            nullable: true
        },
        address: {
            type: "varchar",
            nullable: true
        },
        premium: {
            type: "varchar",
            nullable: true
        },
        status: {
            type: "tinyint",
            nullable: false,
            default: 1
        },
        created_at: {
            type: "timestamp",
            nullable: true,
        },
        updated_at: {
            type: "timestamp",
            nullable: true
        }
    }
})