const { Timestamp } = require('typeorm');

const EntitySchema = require('typeorm').EntitySchema;
const Orders = require('../classModel/orders').Orders;

module.exports = new EntitySchema({
    name: "Orders",
    target: Orders,
    columns: {
        id : {
            primary: true,
            type: "bigint",
            generated: true
        },
        product_id: {
            type: "int", 
            nullable: true
        },
        customer_id: {
            type: "int",
            nullable: true
        },
        delivered_at: {
            type: "varchar",
            nullable: true
        },
        pickup_store:{
            type: "varchar",
            nullable: true
        },
        status: {
            type: "tinyint",
            nullable: false,
            default: 1
        },
        created_at: {
            type: "timestamp",
            nullable: true
        },
        updated_at: {
            type: "timestamp",
            nullable: true
        }
    }
})